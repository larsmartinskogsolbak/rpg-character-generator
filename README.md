A Winforms application that allows the user to create a RPG character. 

A generated character can be a Warrior, Wizard or a Thief. Each class also has an optional subtype/subclass that can be chosen.

Once the character is generated a summary with the character's stats is displayed to the user. 

Each generated character also have simple move and attack methods that can be tested.

The save button saves an .txt file containing an INSERT sql query that can be executed in a database to add the character.