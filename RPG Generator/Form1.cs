﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using character;
using Microsoft.Data.SqlClient;


namespace RPG_Generator
{
    public partial class Form1 : Form
    {
        SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
        
   
        Character chaObject;
        Warrior enemy = new Warrior(3, 4, 3, "Enemy");
        

        public Form1()
        {
            builder.DataSource = "PC7375\\Lars-Martin";
            builder.InitialCatalog = "RPG Database";
            builder.IntegratedSecurity = true;

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (comboBoxClass.SelectedItem == null)
            {
                txtCharInfo.Text = "Choose a class!";
            }
            else if (txtInputName.Text == "")
            {
                txtCharInfo.Text = "Enter a name!";
            }
            else
            {
                string chosenClass = comboBoxClass.SelectedItem.ToString();
                Random rand = new Random();

                if (chosenClass == "Warrior")
                {
                    int str = rand.Next(8, 13);
                    int agi = rand.Next(3, 6);
                    int inte = rand.Next(1, 3);
                    if (chkGlad.Checked == true)
                    {
                        chaObject = new Gladiator(str, agi, inte, txtInputName.Text);
                        picList.Image = imageList1.Images[0];
                        
                    }
                    else
                    {
                        chaObject = new Warrior(str, agi, inte, txtInputName.Text);
                        picList.Image = imageList1.Images[1];
                    }
                    btnMove.Show();
                    btnAttack.Show();


                }
                if (chosenClass == "Wizard")
                {
                    int str = rand.Next(1, 3);
                    int agi = rand.Next(2, 5);
                    int inte = rand.Next(8, 13);
                    if (chkMagus.Checked == true)
                    {
                        chaObject = new Magus(str, agi, inte, txtInputName.Text);
                        picList.Image = imageList1.Images[2];
                        
                    }
                    else
                    {
                        chaObject = new Wizard(str, agi, inte, txtInputName.Text);
                        picList.Image = imageList1.Images[3];
                    }
                    btnMove.Show();
                    btnAttack.Show();
                }
                if (chosenClass == "Thief")
                {
                    int str = rand.Next(3, 6);
                    int agi = rand.Next(8, 13);
                    int inte = rand.Next(2, 6);
                    if (chkNinja.Checked == true)
                    {
                        chaObject = new Ninja(str, agi, inte, txtInputName.Text);
                        picList.Image = imageList1.Images[5];
                    }
                    else
                    {
                        chaObject = new Thief(str, agi, inte, txtInputName.Text);
                        picList.Image = imageList1.Images[4];
                    }
                    btnMove.Show();
                    btnAttack.Show();

                }

                txtCharInfo.Text =
                "Name: " + chaObject.Name + Environment.NewLine +
                "Class: " + chaObject.characterClass + Environment.NewLine +
                "HP: " + chaObject.hp + "/" + chaObject.MaxHp + Environment.NewLine +
                "MP: " + chaObject.mana + "/" + chaObject.MaxMana + Environment.NewLine +
                "Armor Rating " + chaObject.armorRating + Environment.NewLine +
                "Intelligence: " + chaObject.GetInt() + Environment.NewLine +
                "Strength: " + chaObject.GetStr() + Environment.NewLine +
                "Agility " + chaObject.GetAgi() + Environment.NewLine;
                
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            string chosenClass = comboBoxClass.SelectedItem.ToString();
            switch (chosenClass)
            {
                case "Warrior":
                    chkGlad.Enabled = true;
                    chkNinja.Enabled = false;
                    chkMagus.Enabled = false;
                    chkNinja.Checked = false;
                    chkMagus.Checked = false;
                    break;
                case "Wizard":
                    chkMagus.Enabled = true;
                    chkGlad.Enabled = false;
                    chkNinja.Enabled = false;
                    chkNinja.Checked = false;
                    chkGlad.Checked = false;
                    break;
                case "Thief":
                    chkNinja.Enabled = true;
                    chkMagus.Enabled = false;
                    chkGlad.Enabled = false;
                    chkMagus.Checked = false;
                    chkGlad.Checked = false;
                    break;


            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtCharInfo.Text = "";
            txtInputName.Text = "";
            txtMethods.Text = "";
            btnMove.Hide();
            btnAttack.Hide();
            picList.Image = null;
            enemy.hp = enemy.MaxHp;
          
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void txtInputName_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //using(SqlConnection connection = new SqlConnection(builder.ConnectionString))
            //{
            //    connection.Open();

            //    string sql = "INSERT INTO characters (name, class, hp, maxHP, mp, maxMp, armorRating, intelligence, strength, agility) VALUES (@chaObject.Name, @"
            //}

            if (!Directory.Exists(@".\characters\"))
            {
                Directory.CreateDirectory(@".\characters\");
            }
            if (txtCharInfo.Text.Contains("HP"))
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(@".\characters\" + chaObject.characterClass + "-" + chaObject.Name + "-" + "character.txt"))
                {
                   string sql = $"INSERT INTO characters (name, class, maxHp, maxMp, hp, mp, armorRating, intelligence, strength, agility) VALUES ('{chaObject.Name}','{chaObject.characterClass}', {chaObject.MaxHp}, {chaObject.MaxMana}, {chaObject.hp}, {chaObject.mana}, {chaObject.armorRating}, {chaObject.GetInt()}, {chaObject.GetStr()}, {chaObject.GetAgi()})";
                    //writer.Write(txtCharInfo.Text);
                    writer.Write(sql);
                    MessageBox.Show("Character info has been saved to " + chaObject.characterClass + "-" + chaObject.Name + "-" + "character.txt", "Success");
                }
            }
            else { MessageBox.Show("Please generate a character", "Error"); }

        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            Random rand = new Random();

            switch (rand.Next(1, 5))
            {
                case 1:
                    txtMethods.Text = chaObject.Move("north direction");
                    break;
                case 2:
                    txtMethods.Text = chaObject.Move("west direction");
                    break;
                case 3:
                    txtMethods.Text = chaObject.Move("east direction");
                    break;
                case 4:
                    txtMethods.Text = chaObject.Move("south direction");
                    break;
            }
        }
        private void btnAttack_Click(object sender, EventArgs e)
        {
            int currentHp = enemy.hp;
            if (currentHp <= 0)
            {
                txtMethods.Text = "The Enemy is dead";
            }
            else if (chaObject is Magus) {
                txtMethods.Text = "An enemy " + enemy.characterClass + " appears!" + Environment.NewLine + "Enemy HP: " + enemy.MaxHp.ToString() + Environment.NewLine;
                ((Magus)chaObject).Lightning(enemy);
                txtMethods.Text += "Magus casts Lightning" + " for " + (currentHp - enemy.hp) + " damage" + Environment.NewLine;
                txtMethods.Text += "Enemy HP: " + enemy.hp.ToString();
            }
            else
            {
                txtMethods.Text = "An enemy " + enemy.characterClass + " appears!" + Environment.NewLine + "Enemy HP: " + enemy.MaxHp.ToString() + Environment.NewLine;
                chaObject.Attack(enemy);
                txtMethods.Text += chaObject.Name + " attacks for " + (currentHp - enemy.hp) + " damage" + Environment.NewLine
                + "Enemy HP: ";
                txtMethods.Text += enemy.hp.ToString();
            }
        }

        private void txtCharInfo_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "/..";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                        

                    }

                }
            }
            txtCharInfo.Text = fileContent; 
            //MessageBox.Show(fileContent, "File Content at path: " + filePath, MessageBoxButtons.OK);
        }
    }
}
