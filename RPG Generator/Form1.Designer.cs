﻿namespace RPG_Generator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnGenerate = new System.Windows.Forms.Button();
            this.txtInputName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxClass = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnMove = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCharInfo = new System.Windows.Forms.RichTextBox();
            this.chkMagus = new System.Windows.Forms.CheckBox();
            this.chkNinja = new System.Windows.Forms.CheckBox();
            this.chkGlad = new System.Windows.Forms.CheckBox();
            this.btnAttack = new System.Windows.Forms.Button();
            this.txtMethods = new System.Windows.Forms.RichTextBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.picList = new System.Windows.Forms.PictureBox();
            this.btnLoad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(98, 383);
            this.btnGenerate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(148, 52);
            this.btnGenerate.TabIndex = 0;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseMnemonic = false;
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtInputName
            // 
            this.txtInputName.Location = new System.Drawing.Point(406, 112);
            this.txtInputName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtInputName.Name = "txtInputName";
            this.txtInputName.Size = new System.Drawing.Size(325, 26);
            this.txtInputName.TabIndex = 2;
            this.txtInputName.TextChanged += new System.EventHandler(this.txtInputName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(402, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            // 
            // comboBoxClass
            // 
            this.comboBoxClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxClass.FormattingEnabled = true;
            this.comboBoxClass.Items.AddRange(new object[] {
            "Warrior",
            "Wizard",
            "Thief"});
            this.comboBoxClass.Location = new System.Drawing.Point(98, 118);
            this.comboBoxClass.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.comboBoxClass.Name = "comboBoxClass";
            this.comboBoxClass.Size = new System.Drawing.Size(210, 28);
            this.comboBoxClass.TabIndex = 5;
            this.comboBoxClass.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(93, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Class";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(98, 445);
            this.btnClear.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(148, 49);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Reset";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(98, 566);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(148, 83);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save Character";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnMove
            // 
            this.btnMove.Location = new System.Drawing.Point(428, 625);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(110, 34);
            this.btnMove.TabIndex = 9;
            this.btnMove.Text = "Move";
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Visible = false;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(104, 191);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Subtype";
            // 
            // txtCharInfo
            // 
            this.txtCharInfo.Location = new System.Drawing.Point(406, 152);
            this.txtCharInfo.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtCharInfo.Name = "txtCharInfo";
            this.txtCharInfo.ReadOnly = true;
            this.txtCharInfo.Size = new System.Drawing.Size(325, 281);
            this.txtCharInfo.TabIndex = 4;
            this.txtCharInfo.Text = "";
            this.txtCharInfo.TextChanged += new System.EventHandler(this.txtCharInfo_TextChanged);
            // 
            // chkMagus
            // 
            this.chkMagus.AutoSize = true;
            this.chkMagus.Enabled = false;
            this.chkMagus.Location = new System.Drawing.Point(98, 274);
            this.chkMagus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkMagus.Name = "chkMagus";
            this.chkMagus.Size = new System.Drawing.Size(83, 24);
            this.chkMagus.TabIndex = 13;
            this.chkMagus.Text = "Magus";
            this.chkMagus.UseVisualStyleBackColor = true;
            // 
            // chkNinja
            // 
            this.chkNinja.AutoSize = true;
            this.chkNinja.Enabled = false;
            this.chkNinja.Location = new System.Drawing.Point(98, 309);
            this.chkNinja.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkNinja.Name = "chkNinja";
            this.chkNinja.Size = new System.Drawing.Size(70, 24);
            this.chkNinja.TabIndex = 14;
            this.chkNinja.Text = "Ninja";
            this.chkNinja.UseVisualStyleBackColor = true;
            // 
            // chkGlad
            // 
            this.chkGlad.AutoSize = true;
            this.chkGlad.Enabled = false;
            this.chkGlad.Location = new System.Drawing.Point(98, 238);
            this.chkGlad.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkGlad.Name = "chkGlad";
            this.chkGlad.Size = new System.Drawing.Size(100, 24);
            this.chkGlad.TabIndex = 15;
            this.chkGlad.Text = "Gladiator";
            this.chkGlad.UseVisualStyleBackColor = true;
            // 
            // btnAttack
            // 
            this.btnAttack.Location = new System.Drawing.Point(578, 625);
            this.btnAttack.Name = "btnAttack";
            this.btnAttack.Size = new System.Drawing.Size(110, 34);
            this.btnAttack.TabIndex = 16;
            this.btnAttack.Text = "Attack";
            this.btnAttack.UseVisualStyleBackColor = true;
            this.btnAttack.Visible = false;
            this.btnAttack.Click += new System.EventHandler(this.btnAttack_Click);
            // 
            // txtMethods
            // 
            this.txtMethods.Location = new System.Drawing.Point(406, 445);
            this.txtMethods.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMethods.Name = "txtMethods";
            this.txtMethods.ReadOnly = true;
            this.txtMethods.Size = new System.Drawing.Size(325, 146);
            this.txtMethods.TabIndex = 17;
            this.txtMethods.Text = "";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "FFI_knight_(8-bit).gif");
            this.imageList1.Images.SetKeyName(1, "FFI_warrior_(8-bit).gif");
            this.imageList1.Images.SetKeyName(2, "FFI_black_wizard_(8-bit).gif");
            this.imageList1.Images.SetKeyName(3, "FFI_black_mage_(8-bit).gif");
            this.imageList1.Images.SetKeyName(4, "FFI_thief_(8-bit).gif");
            this.imageList1.Images.SetKeyName(5, "FFI_ninja_(8-bit).gif");
            // 
            // picList
            // 
            this.picList.Location = new System.Drawing.Point(320, 752);
            this.picList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.picList.Name = "picList";
            this.picList.Size = new System.Drawing.Size(160, 152);
            this.picList.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picList.TabIndex = 24;
            this.picList.TabStop = false;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(98, 670);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(148, 78);
            this.btnLoad.TabIndex = 25;
            this.btnLoad.Text = "Load Character info";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(815, 964);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.picList);
            this.Controls.Add(this.txtMethods);
            this.Controls.Add(this.btnAttack);
            this.Controls.Add(this.chkGlad);
            this.Controls.Add(this.chkNinja);
            this.Controls.Add(this.chkMagus);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnMove);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxClass);
            this.Controls.Add(this.txtCharInfo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtInputName);
            this.Controls.Add(this.btnGenerate);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "Form1";
            this.Text = "RPG Character Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TextBox txtInputName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxClass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox txtCharInfo;
        private System.Windows.Forms.CheckBox chkMagus;
        private System.Windows.Forms.CheckBox chkNinja;
        private System.Windows.Forms.CheckBox chkGlad;
        private System.Windows.Forms.Button btnAttack;
        private System.Windows.Forms.RichTextBox txtMethods;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox picList;
        private System.Windows.Forms.Button btnLoad;
    }
}

